#include <SPI.h>
#if not defined (_VARIANT_ARDUINO_DUE_X_)
#include <SoftwareSerial.h>
#endif
#include <Wire.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LEDBackpack.h>
#include <Adafruit_GFX.h>

#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "Adafruit_BluefruitLE_UART.h"
#include "BluefruitConfig.h"

#define OFFSET -350
#define BUZZ 3

SoftwareSerial bluefruitSS = SoftwareSerial(BLUEFRUIT_SWUART_TXD_PIN,
                             BLUEFRUIT_SWUART_RXD_PIN);
Adafruit_BluefruitLE_UART ble(bluefruitSS, BLUEFRUIT_UART_MODE_PIN,
                              BLUEFRUIT_UART_CTS_PIN, BLUEFRUIT_UART_RTS_PIN);
Adafruit_MMA8451 mma = Adafruit_MMA8451();
Adafruit_7segment matrix = Adafruit_7segment();

const int vibra1 =  6; //vasen värinämoottori/LEDi
const int vibra2 =  5; //oikea värinämoottori/LEDi
const int analogInPin = A3; //kalibrointipinni

unsigned int laskuri = 999; //laskurin lukema
int kalibrointi = 1024; //trimmerillä säädettävä kalibrointi
int kulma = 0; // sensorin lukema
int steppi = 4; // sensorin lukema muutettuna asteikolle 0-8, keskikohta on siis 4
int lastSentLaskuri = -1;
byte lastSentKulma = -1;
int kulma_n1 = -1;

int pystyLaskuri = 0; // kuinka monta mittausta keppi on ollut pystyssä
#define PYSTYRESET 5 // kuinka monta mittausta keppi on pystyssä ennenkuin resetoidaan
bool waitUntilLevel = true;

int dataOutInterval = 100;
long lastDataOutMs = millis();
bool dataRequestMode = false;
bool dataRequested = false;

long vibraClock = millis();
int activeVibrator = -1;
int vibratorOnMs;
int vibratorOffMs;

bool buzzMode = false;
long buzzCounter = millis();
#define BUZZINTERVAL 4000

unsigned int test = 500;

// A small helpe
void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}


void setup(void) {

  pinMode(vibra1, OUTPUT);
  pinMode(vibra2, OUTPUT);
  pinMode(analogInPin, INPUT);
  pinMode(12,INPUT);
  pinMode(BUZZ,OUTPUT);

  //digitalWrite(vibra1,HIGH);
  //digitalWrite(vibra2,HIGH);
  digitalWrite(BUZZ,HIGH);
  delay(200);
  //digitalWrite(vibra1,LOW);
  //digitalWrite(vibra2,LOW);
  digitalWrite(BUZZ,LOW);

  matrix.begin(0x70);
  matrix.println(8888, DEC);
  matrix.writeDisplay();

  Serial.begin(115200);

  mma.begin();
  mma.setRange(MMA8451_RANGE_2_G); //sensorin herkkyys

  Serial.print(F("Initialising the Bluefruit LE module: "));
  if ( !ble.begin(VERBOSE_MODE) ) {
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  Serial.println( F("OK!") );
  Serial.println(F("Performing a factory reset: "));
  if (! ble.factoryReset() ) {
    error(F("Couldn't factory reset"));
  }
  ble.echo(false);
  Serial.println("Requesting Bluefruit info:");
  ble.info();
  ble.verbose(false);  // debug info is a little annoying after this point!

  /* Wait for connection */
  /*while (! ble.isConnected()) {
    delay(500);
  }*/

  Serial.println( F("Switching to DATA mode!") );
  ble.setMode(BLUEFRUIT_MODE_DATA);
  reset();

}

void loop() {

  while ( ble.available() ) {
    /* rumaa varmuuden vuoksi */
    int r = ble.read();
    if (r==0) {
      dataRequestMode = true;
      dataRequested = true;
    } else {
      dataRequestMode = false;
      dataOutInterval = r * 10;      
    }
  }

  mma.read();
  kulma = (mma.x);  // noin -4000+ - 4000+

  
  
  steppi = map(kulma, -kalibrointi+OFFSET, kalibrointi+OFFSET, 0, 8);
  steppi = constrain(steppi, 0, 8);

  Serial.println(kulma);

  if (waitUntilLevel) {

    if (steppi == 4) {
      waitUntilLevel = false;
      laskuri = 8888;
      matrix.println(laskuri, DEC); //lataa laskurin lukema näytölle
      matrix.blinkRate(1);
      //digitalWrite(vibra1, HIGH);
      //digitalWrite(vibra2, HIGH);
      //setVibrators(40, 100, vibra2);

      bool b = false;
      while (laskuri > 999) {
        laskuri -= 11;
        matrix.println(laskuri, DEC); //lataa laskurin lukema näytölle
        matrix.writeDisplay();
        digitalWrite(vibra1,b);
        digitalWrite(vibra2,!b);
        b = !b;
      }
      laskuri = 999;
      matrix.println(laskuri, DEC); //lataa laskurin lukema näytölle
      matrix.writeDisplay();
      matrix.blinkRate(0);
      digitalWrite(vibra1, LOW);
      digitalWrite(vibra2, LOW);
      delay (500);
      

      ble.print('!');
      ble.print('S');
      ble.write('!' + 'S');
      ble.write((byte)0x00);

      delay (1000);

    }
    return;
  }

  matrix.println(laskuri, DEC); //lataa laskurin lukema näytölle
  matrix.blinkRate(0);
  matrix.writeDisplay(); //kirjoita laskurin lukema näytölle

  //nopea värähtely, jakso 120ms
  switch (steppi) {
    case 8:
      setVibrators(90, 30, vibra2);
      break;
    case 7:
      setVibrators(80, 160, vibra2);
      break;
    case 6:
      setVibrators(70, 430, vibra2);
      break;
    case 5:
    case 4:
    case 3:
      setVibrators(0, 0, -1);
      break;
    case 2:
      setVibrators(70, 430, vibra1);
    case 1:
      setVibrators(80, 160, vibra1);
    case 0:
      setVibrators(90, 30, vibra1);
      break;
  }



  if (activeVibrator != -1 && vibratorOnMs != 0 && vibratorOffMs != 0) {
    if ((millis() - vibraClock) > (vibratorOnMs + vibratorOffMs)) {
      digitalWrite(activeVibrator, HIGH);
      vibraClock = millis();
      if (laskuri > 0) laskuri--;
    } else if ((millis() - vibraClock) > vibratorOnMs) {
      digitalWrite(activeVibrator, LOW);
    }
  }

  //resetointi: kun keppi käännetään pystyy, laskuri resetoituu ja alkaa reilun 5 sek päästä rullata
  //olis tavallaan kiva, että laskuri alkais juosta vasta kun kepin kääntää takaisin vaakaan.

  if (buzzMode && millis() - buzzCounter > BUZZINTERVAL) {
    buzzCounter = millis();
    digitalWrite(BUZZ, HIGH);
    delay(100);
    digitalWrite(BUZZ, LOW);
  }

  if (abs(kulma) > 3900) {
    pystyLaskuri++;
    if (pystyLaskuri > PYSTYRESET) {
      if (kulma > 0) {
        digitalWrite(BUZZ, HIGH);
        delay(100);
        digitalWrite(BUZZ, LOW);
        delay(100);
        buzzMode = false;
      } else {
        digitalWrite(BUZZ, HIGH);
        delay(100);
        digitalWrite(BUZZ, LOW);
        delay(100);
        digitalWrite(BUZZ, HIGH);
        delay(100);
        digitalWrite(BUZZ, LOW);
        delay(100);
        buzzMode = true;  
      }
      reset();
    }
  } else {
    pystyLaskuri = 0;
  }

  //if (!ble.isConnected()) return;

  if (dataRequested || (!dataRequestMode && (millis() - lastDataOutMs > dataOutInterval))) {
    if (lastSentLaskuri != laskuri) {
      ble.print('!');
      ble.print('C');
      ble.write((laskuri >> 8));
      ble.write(laskuri);
      ble.write('!' + 'C' + (byte)(laskuri >> 8) + (byte)laskuri);
      ble.write((byte)0x00);
      ble.flush();
      lastSentLaskuri = laskuri;
    }

    kulma = (byte)map(kulma, -4100, 4100, 180, 0);
    int k = (kulma + kulma_n1) / 2;
    if (lastSentKulma != k) {
      ble.print('!');
      ble.print('A');
      ble.write(k);
      ble.write('!' + 'A' + (byte)k);
      ble.write((byte)0x00);
      ble.flush();
      lastSentKulma = k;
      //Serial.println(k);
    }
    kulma_n1 = kulma;

    //    ble.pollACI();

    lastDataOutMs = millis();
    dataRequested = false;
  }

  /* Get a new sensor event */
  //sensors_event_t event;
  // mma.getEvent(&event);


}

void setVibrators(int onMs, int offMs, int vibrator) {
  activeVibrator = vibrator;
  if (vibrator == -1) {
    digitalWrite(vibra1, LOW);
    digitalWrite(vibra2, LOW);
  } else {
    vibratorOnMs = onMs;
    vibratorOffMs = offMs;
  }
}


void reset() {
  ble.print('!');
  ble.print('E');
  ble.write('!' + 'E');
  ble.write((byte)0x00);
  matrix.println(8888, DEC);
  matrix.writeDisplay();
  laskuri = 999;
  matrix.blinkRate(2);
  waitUntilLevel = true;
  digitalWrite(vibra1, LOW);
  digitalWrite(vibra2, LOW);
}


